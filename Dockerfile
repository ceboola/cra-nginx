# stage1 as builder
FROM node:10-alpine as builder

# Copy source files
WORKDIR /app 
COPY . ./

# Install dependencies
RUN npm install

# Build prod
RUN npm run build

# ----------------------------------
# Prepare production environment
FROM nginx:alpine
# ----------------------------------

# Clean nginx
RUN rm -rf /usr/share/nginx/html/*

# Copy dist
COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html

# Permission
RUN chown root /usr/share/nginx/html/*
RUN chmod 755 /usr/share/nginx/html/*

# Expose port
EXPOSE 5000

# Start
CMD ["nginx", "-g", "daemon off;"]